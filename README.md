<!-- git checkout --orphan temp; git add -A; git commit -am "final"; git branch -D main; git branch -m main; git push -f origin main; git -c gc.reflogExpire=0 -c gc.reflogExpireUnreachable=0 -c gc.rerereresolved=0 -c gc.rerereunresolved=0 -c gc.pruneExpire=now gc "$@" -->
<!--https://gitlab.developers.cam.ac.uk/js2597/4a7b -->

# 4A7
### Scripts for environmental impact of aviation coursework

## Instructions

`run.ipynb` to generate model based on Boeing 787-8

## Flight Data

`res*.pickle` files store results from the simulation  

`flights` folder stores historical data used for analysis

![Screenshot](plots/flight_range.png)

